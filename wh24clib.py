import serial
import crc8

class WH24C:
    def __init__(self, uart):
        self.ser = uart

    def __del__(self):
        try:
            super(WH24C, self).__del__()
        except:
            pass

    def _disableSerial(self):
        self.ser.close()

    # Checksum with Modulo 256
    def _calc_checksum(self,data_in):
        sum = 0
        for c in data_in:
            sum += ord(c)
        sum = sum % 256
        # sum_str = '%2X' % (sum & 0xFF)
        return sum
    
    def _return_false(self):
        data_out = {}
        data_out['output'] = False
        return data_out

    def read(self):
        data_out = {}
        data_out['output'] = False

        # Get raw data and its len
        raw_recv = self.ser.readline()
        len_recv = len(raw_recv)

        # data len should be 17
        if(len_recv == 17):

            # Get identify tx type
            identify_tx_type = ord(raw_recv[0])

            # Check identify tx type, it should be 0x24
            if(identify_tx_type == 0x24):

                # Get Security Data
                checksum = ord(raw_recv[16])
                crc = ord(raw_recv[15])

                # data SUM Checking
                recv_without_sum = raw_recv[0:-1]
                sum_cal = self._calc_checksum(recv_without_sum)

                # Compairing checksum data
                if(checksum == sum_cal):

                    # CRC Checking
                    crc8_hash = crc8.crc8()

                    # CRC Calculation with Polynomial = 0x31
                    crc8_hash._table = [0x00, 0x31, 0x62, 0x53, 0xC4, 0xF5, 0xA6, 0x97, 0xB9, 0x88, 0xDB, 0xEA, 0x7D, 0x4C, 0x1F, 0x2E,
                                        0x43, 0x72, 0x21, 0x10, 0x87 ,0xB6, 0xE5, 0xD4, 0xFA, 0xCB, 0x98, 0xA9, 0x3E, 0x0F, 0x5C, 0x6D,
                                        0x86, 0xB7, 0xE4, 0xD5, 0x42 ,0x73, 0x20, 0x11, 0x3F, 0x0E, 0x5D, 0x6C, 0xFB, 0xCA, 0x99, 0xA8,
                                        0xC5, 0xF4, 0xA7, 0x96, 0x01 ,0x30, 0x63, 0x52, 0x7C, 0x4D, 0x1E, 0x2F, 0xB8, 0x89, 0xDA, 0xEB,
                                        0x3D, 0x0C, 0x5F, 0x6E, 0xF9 ,0xC8, 0x9B, 0xAA, 0x84, 0xB5, 0xE6, 0xD7, 0x40, 0x71, 0x22, 0x13,
                                        0x7E, 0x4F, 0x1C, 0x2D, 0xBA ,0x8B, 0xD8, 0xE9, 0xC7, 0xF6, 0xA5, 0x94, 0x03, 0x32, 0x61, 0x50,
                                        0xBB, 0x8A, 0xD9, 0xE8, 0x7F ,0x4E, 0x1D, 0x2C, 0x02, 0x33, 0x60, 0x51, 0xC6, 0xF7, 0xA4, 0x95,
                                        0xF8, 0xC9, 0x9A, 0xAB, 0x3C ,0x0D, 0x5E, 0x6F, 0x41, 0x70, 0x23, 0x12, 0x85, 0xB4, 0xE7, 0xD6,
                                        0x7A, 0x4B, 0x18, 0x29, 0xBE ,0x8F, 0xDC, 0xED, 0xC3, 0xF2, 0xA1, 0x90, 0x07, 0x36, 0x65, 0x54,
                                        0x39, 0x08, 0x5B, 0x6A, 0xFD ,0xCC, 0x9F, 0xAE, 0x80, 0xB1, 0xE2, 0xD3, 0x44, 0x75, 0x26, 0x17,
                                        0xFC, 0xCD, 0x9E, 0xAF, 0x38 ,0x09, 0x5A, 0x6B, 0x45, 0x74, 0x27, 0x16, 0x81, 0xB0, 0xE3, 0xD2,
                                        0xBF, 0x8E, 0xDD, 0xEC, 0x7B ,0x4A, 0x19, 0x28, 0x06, 0x37, 0x64, 0x55, 0xC2, 0xF3, 0xA0, 0x91,
                                        0x47, 0x76, 0x25, 0x14, 0x83 ,0xB2, 0xE1, 0xD0, 0xFE, 0xCF, 0x9C, 0xAD, 0x3A, 0x0B, 0x58, 0x69,
                                        0x04, 0x35, 0x66, 0x57, 0xC0 ,0xF1, 0xA2, 0x93, 0xBD, 0x8C, 0xDF, 0xEE, 0x79, 0x48, 0x1B, 0x2A,
                                        0xC1, 0xF0, 0xA3, 0x92, 0x05 ,0x34, 0x67, 0x56, 0x78, 0x49, 0x1A, 0x2B, 0xBC, 0x8D, 0xDE, 0xEF,
                                        0x82, 0xB3, 0xE0, 0xD1, 0x46 ,0x77, 0x24, 0x15, 0x3B, 0x0A, 0x59, 0x68, 0xFF, 0xCE, 0x9D, 0xAC]

                    
                    recv_without_crcsum = raw_recv[0:-2]
                    crc8_hash.update(recv_without_crcsum)
                    crc_cal = ord(crc8_hash.digest())

                    # Compairing CRC data
                    if(crc == crc_cal):

                        # Recived data is completed
                        try:
                            
                            # Find wind direction, Range: 0- 359 degree
                            if(ord(raw_recv[2]) != 0x01FF):
                                DIR_8 = ((ord(raw_recv[3]) & 0x80) << 1)
                                wind_direction = DIR_8 | ord(raw_recv[2])
                                data_out['wind_direction'] = wind_direction
                            else:
                                print("found invalid value from wind direction")
                                return self._return_false()

                            # Find Temperature, Range: -40.0C -> 60.0C
                            TMP_H = (((ord(raw_recv[3]) & 0x0F) & 0x07) << 8)
                            temperature = float((TMP_H | ord(raw_recv[4])) - 400) / 10
                            # Check invalid value
                            if((TMP_H | ord(raw_recv[4])) != 0x07FF):
                                data_out['temperature'] = temperature
                            else:
                                print("found invalid value from temperature")
                                return self._return_false()

                            # Find low batter status
                            TMP_H_BIT3 = (((ord(raw_recv[3]) & 0x0F) & 0x08) >> 3)
                            if(TMP_H_BIT3 == 1):
                                data_out['low_batt'] = True
                            else:
                                data_out['low_batt'] = False

                            # Find Humidity, Range: 1% - 99%
                            humidity = ord(raw_recv[5])
                            if(humidity != 0xFF):
                                data_out['humidity'] = humidity
                            else:
                                print("found invalid value from humidity")
                                return self._return_false()

                            # Find wind speed, m/s unit
                            WSP_8 = ((ord(raw_recv[3]) & 0x10) << 4)
                            wind_speed = (float(WSP_8 | ord(raw_recv[6])) / 8) * 1.12
                            if(ord(raw_recv[6]) != 0x1FF):
                                data_out['wind_speed'] = wind_speed
                            else:
                                print("found invalid value from wind speed")
                                return self._return_false()

                            # Find gust speed, m/s unit
                            gust_speed = ord(raw_recv[7])
                            if(gust_speed != 0xFF):
                                data_out['gust_speed'] = gust_speed
                            else:
                                print("found invalid value from gust speed")
                                return self._return_false()

                            # Find accumulation rainfall, mm unit
                            rainfall = (float((ord(raw_recv[8]) << 8) | ord(raw_recv[9]))) * 0.3
                            data_out['rainfall'] = rainfall

                            # Find UV, Range: 0uW/cm^2 to 200000uW/cm^2
                            uv = (float((ord(raw_recv[10]) << 8) | ord(raw_recv[11])))
                            if(uv != 0xFFFF):
                                data_out['uv'] = uv
                            else:
                                print("found invalid value from uv")
                                return self._return_false()

                            # Find LIGHT, LUX unit
                            light = (((ord(raw_recv[12]) << 16) | (ord(raw_recv[13]) << 8)) | (ord(raw_recv[14]))) / 10
                            if((((ord(raw_recv[12]) << 16) | (ord(raw_recv[13]) << 8)) | (ord(raw_recv[14]))) != 0xFFFFFF):
                                data_out['light'] = light
                            else:
                                print("found invalid value from light")
                                return self._return_false()

                            data_out['output'] = True

                        except:
                            print("Something wrong while filling datas")
                    
                    else:
                        print("CRC Checking error!")
                
                else:
                    print("Checksum error!")

        return data_out







