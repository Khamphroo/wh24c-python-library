import serial
import wh24clib

w_serial = serial.Serial('/dev/ttyUSB0', 9600, serial.EIGHTBITS, serial.PARITY_NONE, serial.STOPBITS_ONE, timeout=1)
weather = wh24clib.WH24C(w_serial)


while(True):

    data = weather.read()
    if(data['output'] == True):
        print("Wind Direction: "        + str(data['wind_direction'])   + " Degree")
        print("Temperature: "           + str(data['temperature'])      + " Celsius")
        print("Low Batter Status: "     + str(data['low_batt']))
        print("Humidity: "              + str(data['humidity'])         + " %")
        print("Wind Speed: "            + str(data['wind_speed'])       + " m/s")
        print("Gust Speed: "            + str(data['gust_speed'])       + " m/s")
        print("Accumulation Rainfall: " + str(data['rainfall'])         + " mm")
        print("UV: "                    + str(data['uv'])               + " uW/cm^2")
        print("Light: "                 + str(data['light'])            + " LUX")

        break
